/**
 *  Projecte troncal del joc Wordle de DAM1A
 *
 * @author Joel Montalvan Montiel
 */

import java.util.*

val sinColor = "\u001b[0m"
val greenBorder = "\u001b[42m"
val yellowBorder = "\u001b[43m"
val greyBorder = "\u001b[47m"
val redBorder = "\u001b[41m"
val negro = "\u001b[30m"
val rojo = "\u001b[31m"
val magenta = "\u001b[35m"
val blanco = "\u001b[37m"
val cyan = "\u001b[36m"

val words = arrayOf("LENTO", "ACERO", "ABEJA", "PEAJE", "DOLOR", "PACTO"
    , "AUTOR", "BARON", "BOLSA", "DONAR", "PESCA", "METAL", "JOVEN", "POETA"
    , "SOBRE", "CLASE", "VOCAL", "SUTIL", "HIELO", "TRIGO", "MULTA")

fun main() {
    println(greenBorder+negro+"""
        ___________________________________    ______________________    _______________________   ______________________   _______________________   _______________________
        |                                 |    |                    |    |                     |   |                    |   |                     |   |                     |
        | ____         _____         ____ |    |      ________      |    |   ______________    |   |    __________      |   |   _______           |   |   ______________    |
        | \   \       /     \       /   / |    |     /        \     |    |   |    ____     |   |   |   |  ______  \     |   |   |     |           |   |   |   __________|   |
        |  \   \     /   _   \     /   /  |    |    /  ______  \    |    |   |    |___|    |   |   |   |  |     \  \    |   |   |     |           |   |   |   |             |
        |   \   \   /   / \   \   /   /   |    |   |   |    |   |   |    |   |      _     /    |   |   |  |      |  |   |   |   |     |           |   |   |   -------       |
        |    \   \_/   /   \   \_/   /    |    |   |   |____|   |   |    |   |     | \    \    |   |   |  |      |  |   |   |   |     |________   |   |   |   _______|      |
        |     \       /     \       /     |    |    \          /    |    |   |     |  \    \   |   |   |  |_____/  /    |   |   |             |   |   |   |   |__________   |        
        |̣      \_____/       \_____/      |    |     \________/     |    |   |_____|   \____\  |   |   |__________/     |   |   |_____________|   |   |   |_____________|   |
        |                                 |    |                    |    |                     |   |                    |   |                     |   |                     |
        |_________________________________|    |____________________|    |_____________________|   |____________________|   |_____________________|   |_____________________|
    """.trimIndent()+sinColor+"\n")
    val scanner= Scanner(System.`in`)
    println(sinColor+magenta + "Introduce tu nombre de usuario (SIN ESPACIOS)")
    val username = scanner.next()
    println(magenta + "Bienvenido a Wordle $username!!!")

    println("Sabes las reglas del juego?")
    instruccionesDelJuego()

    println(magenta + "Introduce una palabra para comenzar la partida (5 LETRAS):" + sinColor)
    logicaDelJuego()

    reinicioDelJuego()
}

/**
 * Esta funcion pregunta al jugador si desea recibir las intrucciones del juego o comenzar la partida
 *
 * @param question Es la pregunta que se le hace al jugador para saber si explicarle las reglas del juego
 */
fun instruccionesDelJuego (){
    var correct= false
    while (correct==false){
        val scanner = Scanner(System.`in`)
        val question = scanner.next()
        if (question == "NO" || question == "no" || question == "No") {
            println(magenta+"No te preocupes, las reglas del juego son sencillas de entender:");
            println(magenta+"El jugador escribe en la primera línea una palabra de cinco letras de su elección e introduce su propuesta.");
            println(magenta+"Después de cada proposición, las letras aparecen en color:");
            print(cyan + "- El fondo ");print(greyBorder + negro + "GRIS");println(sinColor + cyan+" representa las letras que no están en la palabra buscada.");
            print("- El fondo ");print(yellowBorder + negro +"AMARILLO");println(sinColor + cyan+" representa las letras que se encuentran en otros sitios de la palabra.");
            print("- El fondo ");print(greenBorder + negro +"VERDE");println(sinColor + cyan+" representa las letras que están en el lugar correcto en la palabra a encontrar");
            println(magenta + "El juego finaliza una vez adivinada la palabra, pero cuidado porque DISPONES DE SOLO 6 INTENTOS para ganar la partida.");
            println(magenta + "Dicho esto, es hora de comenzar la partida.")
            correct=true
        }
        else if (question == "YES" || question == "yes" || question == "Yes" || question == "SI" || question == "SÍ" || question == "si" || question == "sí" || question == "Sí" || question == "Si"){
            println("Bien, pues hora de comenzar la partida.")
            correct=true
        }
        else {
            println("No te he entendido, vuelvo a repetir:")
        }
    }
}

/**
 * Esta funcion analiza la palabra escrita por el jugador, además de ir añadiendole los diferentes colores dependiendo de su posicion,
 * y también se encarga de ir restando los intentos para finalmente saber si ha ganado o ha perdido el usuario.
 *
 * @param respuesta Es la palabra introducida por el jugador para divinar la palabbra secreta
 */
fun logicaDelJuego(){

    val randomWord = words.random()
    var attempts = 6

    do {
        val scanner= Scanner(System.`in`)
        val respuesta = scanner.next().uppercase()
        var letter = 0
        if (respuesta.length == 5) {
            for (i in respuesta) {
                if (randomWord[letter] == i) {
                    print(greenBorder + negro + i + sinColor)
                }
                else if (randomWord.contains(i)) {
                    print(yellowBorder + negro + i + sinColor)
                }
                else {
                    print(greyBorder + negro + i + sinColor)
                }
                letter++
            }
            attempts--

            print("\n" + rojo + "Te quedan  $attempts  intentos" + sinColor +"\n")
        }

        else println(redBorder + negro + " La palabra introducida no es de 5 letras " + sinColor + "\n" + "Introduce una palabra de 5 letras, porfavor")

        if (respuesta == randomWord) {
            println(greenBorder + negro + "CONGRATULATIONS! " + sinColor + " Has podido adivinar la palabra!!! Que locura por tu parte crack!!!")
        }

        else if (attempts <= 0 && respuesta != randomWord) {
            print(redBorder + negro +"Lo siento, la proxima vez piensa antes de malgastar tus intentos.")
            print(" La palabra era " + blanco + randomWord + negro + "." + sinColor + "\n")
        }
    } while (attempts - 1 >= 0 && respuesta != randomWord)
}

/**
 * Esta funcion, una vez finalizada la ronda, pregunta al jugador si desea empezar otra ronda o concluir el juego
 * @param reinicio Es la pregunta que se le hace al jugador para saber si quiere reinicira la partida
 * @param respuesta Es la palabra introducida por el jugador para divinar la palabbra secreta
 */
fun reinicioDelJuego(){
    while (true){
        println(magenta + "Quieres reiniciar la partida?")
        val scanner= Scanner(System.`in`)
        val reinicio = scanner.next().uppercase()
        if (reinicio == "SI" || reinicio == "SÍ" || reinicio == "YES"){
            println(magenta + "Introduce una palabra para comenzar la partida (5 LETRAS):")
            logicaDelJuego()
        }
        else if (reinicio == "NO"){
            println(magenta + "Gracias por jugar, suerte con tu vida!!!")
            break
        }
        else println("No te he entendido, vuelvo a repetir:")
    }
}
