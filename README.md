# Wordle_JoelMontalvan

## Descripción del Proyecto

Wordle es un proyecto creado por Josh Wardle, un ingeniero de Reddit. De hecho, el nombre del juego es una combinación de palabras de su apellido, al que le puso una o en lugar de una a para que aparezca el término "word", que en inglés significa palabra. Wordle fue lanzado a mediados de octubre del 2021, aunque ha sido a principios del 2022 cuando ha alcanzado la viralidad.

Wordle es un juego de adivinar palabras, que tiene un formato de crucigrama y con similitudes con otros juegos como el Mastermind. En él, tienes que adivinar una palabra en seis intentos, en los que no se te dan más pistas que decirte qué letras de las que has puesto están dentro de la palabra.

Su mecánica es muy simple, y posiblemente a esto se debe parte de su éxito, ya que hace que sea accesible para todos. Cada vez que haces un intento, tienes que escribir una palabra válida, y se te dirá cuáles de las letras introducidas están, cuáles no, y si están en el sitio donde les toca o no. Cuando hagas un intento verás los resultados de ese intento, y podrás volver a probar.

## Como instalar y ejecutar el proyecto

Descarga el proyecto en formato ZIP para después exportalo donde te vaya mejor en tu sistema operativo, a la vez, debes tener instalado un programa para poder ejecutar el proyecto, por ejemplo Intellij IDEA.

Una vez descargado el proyecto, debes abrirlo en el programa que tengas en tu sistema operativo. Y deberas esperar unos segundos a que se ejecute el proyecto con el juego.

Una vez instalado, ves hacia el boton del play y ya podras disfrutar del juego. SUERTE!!!

## Instrucciones del uso del juego

Cuando lo hagas, lo primero que verás será la explicación con las normas. Es sencillo, tienes que escribir una palabra y ver las letras que has acertado, que tendrán diferente color dependiendo de si acertaste:

Dependiendo de cada proposición, las letras aparecen en color:

- El fondo gris representa las letras que no están en la palabra buscada.
- El fondo amarillo representa las letras que se encuentran en otros sitios de la palabra.
- El fondo verde representa las letras que están en el lugar correcto en la palabra a encontrar.

Tienes seis filas de palabras, lo que quiere decir que tienes seis intentos para adivinar la palabra correcta. No hay límite de tiempo ni ningún tipo de presión, simplemente tienes que ir escribiendo las palabras que creas que pueden ser. Al principio, puede ser útil no intentar adivinar con las dos primeras, sino simplemente ir utilizando letras en palabras para ver cuales están, aunque aquí esto ya depende de la estrategia de cada uno.

Una vez finalizada la partida, se podra jugar tantas veces como al usuario le apetezca.

## License

MIT License

Copyright (c) 2022 joel [itb] montalvan montiel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

